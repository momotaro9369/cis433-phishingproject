from requests import session
from time import sleep

def main():
    with open('randomDict.txt') as f:
        myList = f.read().splitlines()
    
    for i in range(0, len(myList), 2):
        payload = {
            'username': myList[i],
            'password': myList[i+1]
        }
        print "Sending: " + myList[i] + " " + myList[i+1]
        with session() as c:
            site = 'https://ix.cs.uoregon.edu/~duyeda/cis433/phishingLogin.php'
            c.post(site, data=payload)
        sleep(1)
   
if __name__ == "__main__":
    main()

