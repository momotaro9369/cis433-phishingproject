# README #
CIS 433 Phishing Project  
Authors: Douglas Uyeda, Logan Donielson  
An Anti Phishing Project that was presented at the 2016 Oregon Cyber Security Day.
The project demos an anti phishing method which invovles spamming a phishing site with fake login information.
The outcome is that the phisher will not be able to distinguish between legitimate and fake data.

To view the poster click on the PDF File:
Donielson_Uyeda_Final_Poster.pdf

## Requirements
* python 2
* requests

